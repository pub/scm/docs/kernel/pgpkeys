pub   rsa2048 2009-05-20 [SC]
      FD9CE2D8D7754B78AB279BBD2C3B436FEAC68101
uid                      Adam Borowski <kilobyte@angband.pl>
sub   rsa4096 2017-08-31 [S]
      9236557B170C87F8821C0AC3C1E0D92E986F7C7E
sub   rsa4096 2017-08-31 [E]
      5E7ECB13AC3AE0531010800DEE950172E20EC2A6
sub   rsa4096 2017-08-31 [A]
      B8925D35780B6265C1208DBCA879D6DCCCFA16EB

-----BEGIN PGP PUBLIC KEY BLOCK-----

mQENBEoT50gBCADRZ2VhVz0+LQ9Y8QKEaHiqfBc/AOIEQF/amRPLWikL76zQYLFp
+CQO1zcKSwOpzWSEq7eIDk6uSUvodVSkc7K3EZ9T/IDheflROXVF0ykar+p/y9Tu
jQh3NT7aq7vnoltuW8e2AWIu0tFeARD+eAtSht+/7mwfIEDeNj+71XTdcwJd2MFQ
+UagLZs6tQBDCfjfQSxbkGUtAppg3jd83s9PD8cfM0hl1liXUHfNFNHfB5csmjGP
50bmmrzwiGTayBFhGPKT3MvMibrltHAr4acwtyzODALFeGk4SzuD7zntHvp827RI
tM0Jy0XdAhiWaSWXbxstwStVaB0mk1qRUmElABEBAAG0I0FkYW0gQm9yb3dza2kg
PGtpbG9ieXRlQGFuZ2JhbmQucGw+iQE3BBMBCAAhBQJKE+dIAhsDBQsJCAcDBRUK
CQgLBRYCAwEAAh4BAheAAAoJECw7Q2/qxoEBHckH/jN07iWEkk9KE+nhmDD2odpH
ZHYIzpkau3dW65+F7pDmHTRWvBwY7TMe00pb0mI32TRBQX8Py+qjnz7e8ejcGZxc
m/Hi9yT7eTANjLVBJjl9om6JbMLVkZhml0I/PYImy6HeHj5NsOBDSbq+H15bGpBs
xOZky44MwCLH17Zi0SaY26lFJj3h4phNgZeTHeMXVuv0uWBlZaJPHhcvnLs8VEOf
4j/IqJelRwG2Xa34Gsysav19rbnVffr0JPmGg21vz7Y23a8HmnenQBnbGYGSZ/P7
4sl+OkG4fgcbqVhYR/v5fZGoq57niLeN5YAMSN4b4V0D+LDjpYOxYvIC8JwU4YKJ
AhwEEAECAAYFAlISZGYACgkQ2O7X88g7+prUkw//WBSUf5Dzkc4/d9+awCNY36an
QdYGdJaGJNBtW69Fn0tw3cPwiU+TlKJEkB3yUCdFhTdwfeoC8PNYG5Ha0sUIZsab
O2wSWd+YM72DJ8eeQSLUksXZVMl1/0ufEN2PMuxoM901d1+64NHRDDYEQMNe3kDR
Hgkg97z318xoRfdEXg43Euu50sknVNfYLsgk95UvpKX//mMcVAnIfQ8L801cpMaj
2Bjf5tnCYVdpS2FoygpfKhofL+VrAXf+kgw3SFKRVZiSWDhYQVbMBwa4ejsi1nag
xL3hj88zzyi1u3MhvONGJNaq5X/li6VQBVYuQ4Z6OWUUsYlNe+UusGJSDkz25GX7
qHwkUjDH5lgd3MiLv0o+DlobGJ9Xm1lSFN2EKgIUzgf/7poqyxSYRChyjP66+oy8
krpZoK6haIuavtaUclFqffKxySC4QOElYefKOTuiWwy72m0K6QWN/3BvfXUimaG1
6JnL2sTVlVcLLHzefgSTMZBOrLRQ60Y6dqKMrD28fYm16vp1yQbShsraWoQ/vV6y
h22sQA1zKy84kCNRIBK8E/7S+siDYeLFw9YpCHh5YhnzDE6Trq1N5ZC+OKqHRRoq
3vosoZdPK493dZ2jhsF9QB59tKkkXve/q91DzmjhNQuP85o1AbzV9zWs+hfvXP50
pOYqOEJ1hh8/d0DnAIu5Ag0ETAggCgEQAMgnnzE/NUxHcBhkKCy+l+8cWQzeR+j8
KFI3f+cF+M9MEE9Q+yVqFuwOEIKQXTFDBOoY9aVsSlmQs7IR1SXqJvhUPJlaAxyc
RSuwaokvyV7XsBC6eXpiV1KbrziooHTzV81zkKPaBlaYVPmHV9t3INd07sw8bBUx
3s+LnsAQg+tAUDoIHqppeASNzWrFc1BWuiXPZqFDs3SxhMWmUZHtE9b5XPuEtoI4
59u4PV1fq1FNyCFYhCsRmybNOgT/i4e/DrSpMwzHIF7afmIa/Gj1/h2G7AizKzd9
oWPAOzCESl8IgyW1E8Ns/BDVCepCsPjzjBbgD/IXjXni8/OLhuFpeu9vnzqwv/fh
487ShvvQqnwNNnxu5Cq8dJlzHG6Libc8uT/tUuM6+1EPT/rBmObOdUc6Q3IRhGBB
+GI4BdIj7ZnU0HqCjGNQ9twN8H3NnWJeMbLJMuC1rfHWxaI9yAl0wwAbYpnIwjmv
Jjbcu66CKUQTM+e8hKyCFhBbC8JOpGuSfuwVlXsLkoGb/GdVgHYV/Nr7GXtElvW+
BG86Ao+zrU5GAPRG0FwoHRPZbEdTWZusmOJTipF5UvH9LmKdeRGR0LIs1CAG3F5D
Tut1/W3Ann9LPruirHQznGqDX6IQfTt5A4mLFOEagX+7G1h8Q4VAf4O3dqWa26/e
FY5Dudep3IbXABEBAAGJATwEGAECACYCGwwWIQT9nOLY13VLeKsnm70sO0Nv6saB
AQUCWirpCwUJDiQagQAKCRAsO0Nv6saBAdj/B/sG1cm2Qf3HwTTnNUbHCvPZDOmI
TstcNP0oDkng40jcgY0wUsbBcef+7hA54X5P7Kq5rXuBl5h2u06udACYL+vhjSy9
9KeEYE0Yoow8LzqrQZ+6ftWTHCJYJLIZnXNznBYSmDqWRgCtjoKCgaS2g4Rum4QV
WHJeTFT20S8t4uB4VFEdv+d0/uYLBFA8q5HWVcx1tE1GLa37J9vvo4bo6AtIUAO1
uGqqmOQArXWpVl02ejh8rx9cMSnxnaEdbXPIK+WiJVBzGJkVowcrnTMlexcOhymr
RiM/lkAQXmZP39P0kQOXIBe56GD5Kx0L7/VHb2AAr953olx8ivSdth3aL+M5uQIN
BFmogwoBEAD4zATcIMPFjyMMYZjygqwMgVZSCuFAW/2odOUABxdEnqsvUpMaJbO+
UzF3CvJU17+RagQbvZoTHCvl+nOamJ7vHg/4IqEzokIwLkwUw5ZEdYMrjDusyyEa
BdDCth16cpviVgqqRUd5CcAn3XjUngRRPuF+rP20ItnFcYL4hSslDhenvH0Hcano
fxyxYy4bcvNbtG//nFprlNzWezaUL8MUu/Um0nkocR2b0KmlPS8m0t1zseTjnCAM
Kil36H1c34E+lNCV+yVpmmJ6LAoIlwJq0/78a20yeYewezdZJp9YAjK4+oUdAjWg
dM6NXP//FMuD81CsR4StSbELcyBC2s7qws1XkF2RiGiUBT8cCEcvXBjSw2Tf1PSM
5qeB+5lJhsBmNUNAAuZpxIaCIkH60WynQfKGt72k/uD6UtuLX9yu+LKmNXbR+5R9
qqbsN3nI+hICMo3sNsV2oD18whW7Yr7Spb37Dk4l2T53Pz876kXg3bqFaer6X0tk
SP/K5y9HCjr4BIZ+icmIsCeuyiVXDhuWk6DdZh4E3WssPcfTPHGVc0DZW48y6mUO
OH19OQSyN3Ss0hF3MLbkxKF0cBSTJC9itgXyO8uKX4Xfi9AoWknvVRoWTWMfaAG0
Ll2Uaxz2XLSmYRkc4rYnRa5e4XV9hBzkI4t5GMg41wMK/bK9a54P5wARAQABiQNs
BBgBCgAgAhsCFiEE/Zzi2Nd1S3irJ5u9LDtDb+rGgQEFAlosOsICQMF0IAQZAQoA
HRYhBJI2VXsXDIf4ghwKw8Hg2S6Yb3x+BQJZqIMKAAoJEMHg2S6Yb3x+emwP/1Vu
aDYxuykr4liah/KVQvifQWCvI+ENmlhyZfWn68anevPRubiPZ5nRzGObc4vxIKxN
VYxW8sQgUEgZqwTNTg96QcNMMN8fk7j9KZjChi7TYzTH5uYn1WhgrxV1no8TzViX
ZcDWYFuRMO9xCAIIMGYW9alfl6TcIIyWbl9JQj8aZ/XCi14zLrFiQEg1IXDCK063
SxZGM2OaorRt7BwiiwLWO+IjNMvIqR6eZeOj1XppgPVmsMPLP9QmxQKJERmSbhGS
NmAbcBU6itdf099fw0uenXxtVnXpSly1n764cVlxW4aupaZXUhR60eXkY5bZZFay
1twWRC+Sh4YKP6OoyhWHXNT5lzhXygEzdshoLCvPcQ+nPEnEpemH8oPrYCQiIUPh
jjpCsFk5rPQ7tLvgPRq9svMqO8z4EqBXK1cjVYcAca0GiHZV/2v4dUaQQfsEEIBH
qIFWagrKPfoEJoFXFYjIzYwZiWcUANphqs9RdkOYBtY3qRVbu23JFqUu90pygs0x
Fgxpd0goim3nuSRcUJvY3+ag/SxhZskjDJXoQxFsSsnxwvfGGj3oaYRLtSQEOPFk
7qP8o6vLxwxa8M4gC+VsV3t4jwXpJ4y5M9axBwBecUWId86Mc8WQKen3RBeli30L
bqoCM2KYAiOYLfBjyR8cMmKbc/vmrLXIz8/bGiOXCRAsO0Nv6saBAYSbCACL2PnE
cdePc4s5ufFDxy5ITQN8QrdFbQIOoUISwPMAnmx1fyYU516IzioIRP7KArGsLllc
p1r+PIOw0GZfSps7wl0BLP9N4lZtgyUyyyuVOFGnUzs03Gvrk75rEgy4NixO1Ont
YgWVVph7JZFm08qPYgBM193RLnhvddeyuxNUg5TbIlfXaFe74BSOD2ScqtBvx63c
mlJVCxlQuhXWDkyfK+SvMG+O87P9/HXG3zYW83ZrK92KNosZW/G6iGHO2hqgenuC
OE41NDfw0Gi1BF8u9XZO6UqBTR8LrZAyHz0XS+XqmTMqBZQ1+5qAJfYrDVpsPSLI
kK4aiJFn4sHmLdemuQINBFmog70BEADCYEzJndzSQx5etSytdwUV8/0pamTqNAFZ
SlSzyAmFndvEG4F76zniLjhfawADZmGUQ8GzbNGdKNwefuUbW0lfFKSp3jme1OhH
fyWZ+CbziHpJWjw7Mj02x0zylggl6e9zJ2EOmM+Oa05qS4y0CIsUuc7Xjj7giUws
UOjEFp21GXylbehMzUP7jYIsA0l24Nh2lZIvs/WA9RssjOxcgWJ+Aw9U7YtDY8s3
crBtx7Hx2b3Ollb7Ac6sQGqQaHHKll1FkPRdcq4JZmzyWIdweaRHrDTGXai4laAI
7Grp8gLLyVRzxzmpGmIxpXlwz4AfcSh0UIEV0EjEXbRUcxvInznoZwDYgh0umaen
3xAUeH0qLp6+B5mZhYNCjmW7ibE14qNsyNxs15pRNqfFwJCOxWU4+Oel8rCspKs4
RPFTmi7k6MC/eU10e8F5BIpILMHPrmV+CHX4VCPvqhiNuIC3LlsenyM0mqHi6Lt3
C0+kdxMOnQUy/H7V6zMY/7PNd+jNd8qBZovyRvdBcZFEei9JjCHiYe0Vi/UAOAOK
O4CJ2PkfI73EgvsgsWjPFgptw1uhqULPfnuTU81Lt4DGdQIHLclcMrrgA5Pc6NDs
/NWiDdBQIAbaKctwAGD/USJicFytwJRiRn9/p1U2BRr8laFeJRITtvn8cvtnDHhH
2OcQQzpU5wARAQABiQE2BBgBCgAgAhsMFiEE/Zzi2Nd1S3irJ5u9LDtDb+rGgQEF
AlosOsIACgkQLDtDb+rGgQEq3QgAy4kP+G6gcxVMMfXlN/6IshNoODQGDaiJoJx8
VHDJEy1Sc7XFHVICXpRyosY9FpYmiG40g+9qAO1P/tVh6a2gC4QHShQMomctMXsK
u+PndxA80QZ8ca6ia2embEwwVMaUaRyJ1lzWGgkbG9oOodgL1p9cYhIIVLttZnTF
GegAVghB81Jpty4xJfPg5vnO39+kDjcMiSZtBY9Pvkg3yEpbcEwxJNIdw7J1H9aq
7B1i+qtlAXuRv9EHnfvV2NKB7qFRCaDg7yqQkHZ4g9EFCFotnzuYgyKuiY5Tx2cl
erMX6voofYFooom3F5+oJFS1IN0bxOp1FSdy3q6g/lokE6fiObkCDQRZqIRmARAA
vA511fAryvb2I7mk0eDjhiYbjyGDH9O9jGgFyNiV+lv19eeJ5a3uH3jhqm8gDgSp
filzOw6hY6ousq047QZQmgUuKGaiR7nxOkfpV4Eai1UT1KWXi88krqZ1LIApmYfn
roRvbB5BF9Ys3W9UtsPxMd/OZM2Qpt5kBJ+KBxYUKqiyOJV9wSb5uHA+UPcO7HzE
5BqGd6jwKwhZ2VA+zrqM+0HgT4k+l/nXypdWciOgIqmpsZZ04/wrcSvY5YpfYrWO
TE4D0SLE4CBLVViVpmFbcLbkxc0LmC2lKBKP1qE42jDK5IVyg62cQ8QP+GwI/Xe9
emRsy1z9MXO6kCASWibOTfVu7tP0LlQGTcN9fPrS1zYTW1KWSegQIWgTF6poslPb
L1kQS4cE8DtkmMkSqbVN35s5pUFiHK6onJA9OKAmxeFh454sKQ4E/3VCkWTQ1sQw
NXX/ckPWiknYjxFB1TVxzZtrsFRKv0cj3CEcnhzWzmDx5oXH5T7OiMxuv3FCAwq7
ZPBzmkqsqZBMhMJf+LDmX/Y+Dws9JKKzbl3NfgREpjmy9Ng4LaLEZxJjPNrtBKo3
hWAR7ZRuUfWBIDSCSsW7ObSQQXWCvOhnhrH8Gh16C35RlOqKiT9LIetdI6cdSp/n
4LQY17arPbtnc5EQyO72HaNO9EZv34DaezSDAOG1RDkAEQEAAYkBNgQYAQoAIAIb
IBYhBP2c4tjXdUt4qyebvSw7Q2/qxoEBBQJaLDrCAAoJECw7Q2/qxoEB9D0IALQI
intBHbsAB+PvPHUxs9j7XhDIuVc6qs9hVmk/xeebix3M68/WWRNlvGOiPsmduRp9
CUfnQkImfVNc6WC0YgqOn44x0+SUTn37UrJA/PG61kF8pMaRz3cRLvJuS6byIlbX
BRAWs84HgF//iCxqYt8HTiA4dzrtTg+8lzOuQIrR9AHb91Xm+0/Rz0iR01K0W3qF
TJbWqdUy5vqhE1f+8LWeXCbFnCZ18wNj20jg9+kvzVgdq5JLbEv5kFO5FyGtgkGd
oOIcjPFwSuKe2vjQkirumcc0GAExZTmzt1HzrFDIdhjbDpbCcztVOAx4pCSgGzaI
llkec1lUfaG9fV/wa84=
=2ToW
-----END PGP PUBLIC KEY BLOCK-----
